<?php

require_once dirname(__DIR__) . "/api/vendor/autoload.php";
require_once __DIR__ . "/vendor/autoload.php";
set_error_handler("ErrorHandler::handleError");
set_exception_handler("ErrorHandler::handleException");
$dotenv= Dotenv\Dotenv::createImmutable(dirname(__DIR__) . "/api");
$dotenv= Dotenv\Dotenv::createImmutable(__DIR__ );
$dotenv->load();

header("Content-type: application/json; charset=UTF-8");
