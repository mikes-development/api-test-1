<?php
    class TaskController {
        public function __construct(private TaskGateway $gateway) {


        }


        public function processRequest(string $method, ?string $id): void
        {
            if ($id == null) {
                if ($method == "GET") {
                    echo json_encode($this->gateway->getAll());
                }
                elseif ($method == "POST") {
                    $data = (array) json_decode(file_get_contents("php://input"), true);
                    $errors = $this -> getValidationErrors($data);
                    if (! empty($errors)) {
                        $this->respondUnprocessedEntity($errors);
                        return;
                    }
                    $id = $this->gateway->create($data);
                    $this->respondCreated($id);
                }
                else {
                    $this->responseMethodNotAllowed("GET, POST");
                }
            }
            else {
                $task = $this->gateway->get($id);
                if ($task === false) {
                    $this->respondNotFound($id);
                    return;
                }
                switch ($method) {
                    case "GET":
                        echo json_encode($task);
                        break;
                    case "PATCH":
                        $data = (array) json_decode(file_get_contents("php://input"), true);
                        $errors = $this -> getValidationErrors($data, false);
                        if (! empty($errors)) {
                            $this->respondUnprocessedEntity($errors);
                            return;
                        }
//                        echo "update $id";
                        $rows = $this->gateway->update($id, $data);
                        echo json_encode(["message" => "Task updated", "rows" => $rows]);
                        break;
                    case "DELETE":
//                        echo "delete $id";
                        $rows = $this->gateway->delete($id);
                        echo json_encode(["message" => "Task deleted", "rows" => $rows]);
                        break;
                    default:
                        $this->responseMethodNotAllowed("GET, PATCH, DELETE");
                }
            }
        }

        private function  respondUnprocessedEntity(array $errors): void {
            http_response_code(422);
            echo json_encode(["error" => $errors]);
        }

        private function responseMethodNotAllowed(string $allowed_methods): void {
            http_response_code(405);
            header("Allow: $allowed_methods");
        }

        private function respondNotFound(string $id):void {
            http_response_code(404);
            echo json_encode(["message:" => "Task with ID $id not found"]);
        }

        private function  respondCreated(string $id): void {
            http_response_code(201);
            echo json_encode(["message" => "Task Created", "id" =>$id]);
        }

        private function getValidationErrors(array $data, bool $is_new = true): array {
            $errors = [];
            if ($is_new && empty($data["name"])) {
                $errors[] = "name is required";
            }

            if ( ! empty($data["priority"])) {
                if (filter_var($data["priority"], FILTER_VALIDATE_INT) == false) {
                    $errors[] = "priority must be an integer";
                }
            }
            return $errors;
        }
}
