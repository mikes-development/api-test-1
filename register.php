<?php
	require_once __DIR__ . "/vendor/autoload.php";
	if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
		$dotenv->load();

		$database = new Database($_ENV["DB_HOST"], $_ENV["DB_NAME"], $_ENV["DB_USER"], $_ENV["DB_PASS"]);
		$conn = $database->getConnection();

		$sql = "INSERT into user (name, firstname, lastname, emailaddress, username, password_hash, api_key)
			VALUES(:name, :firstname, :lastname, :email, :username, :password_hash, :api_key)";

        $stmt = $conn -> prepare($sql);
        $password_hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
        $name = $_POST["firstname"] . " " . $_POST["lastname"];
        try {
            $api_key = bin2hex(random_bytes(16));
        } catch (\Random\RandomException $e) {
            echo "Error: $e";
        }
        $stmt->bindValue(":name", "$name");
        $stmt->bindValue(":firstname", $_POST["firstname"]);
        $stmt->bindValue(":lastname", $_POST["lastname"]);
        $stmt->bindValue(":email", $_POST["email"]);
        $stmt->bindValue(":username", $_POST["username"]);
        $stmt->bindValue(":password_hash", $password_hash);
        $stmt->bindValue(":api_key", "$api_key");

        $stmt->execute();
        echo "Thank you for registering. Your API key is ", $api_key;
        exit;

    }
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Register for API Key</title>
		<link rel="stylesheet" href="https://unpkg.com/@picocss/pico@latest/css/pico.min.css">
	</head>

	<body>
		<main class="container">
		<h1>Register</h1>
			<form method="POST">
				<label for="firstname">First Name
					<input name="firstname" id="firstname">
				</label>

                <label for="lastname">Last Name
                    <input name="lastname" id="lastname">
                </label>

				<label for="username">Username
					<input name="username" id="username">
				</label>

                <label for="email">E-mail Address
                    <input name="email" id="email">
                </label>

				<label for="password">Password
<!--					<input type="password" name="password" id="password">-->
                    <input type="password" id="password" name="password">
				</label>

				<button>Register</button>
			</form>
		</main>
	</body>
</html>
